# Monitoring

When it comes to systems, we need to verify that they work, and also find issues and repair them in future releases, thus we monitor all in 3 main levels:

- Infra Monitoring: (one of them more then enough, yet knowing all of them can serve you in different types of system)
    - prometheus+grafana
    - nagios/check_mk
    - datadog
- Appilcation Monitoring
    - datadog
    - Prometheus
    - ELK-Stack
- Log Monitoring
    - ELK-Stack

